package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word");
        String word = scanner.nextLine();
        System.out.println("Enter the number k (0 <= k <= " + (word.length() - 1) + ")");
        int k = scanner.nextInt();
        char result = charAtWord(word, k);
        System.out.println(result);
    }

    public static char charAtWord(String word, int k) {
        return word.charAt(k - 1);
    }
}
