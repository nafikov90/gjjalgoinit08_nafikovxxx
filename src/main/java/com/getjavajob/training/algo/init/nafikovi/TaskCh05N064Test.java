package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh05N064.averageDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        densityTest();
    }

    public static void densityTest() {
        int[] testPopulation = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
        int[] testSquare = {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
        assertEquals("TaskCh05N064Test.densityTest", 2, averageDensity(testPopulation, testSquare));
    }
}
