package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N033.lastDigitIsEven;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N033.lastDigitIsOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        lastDigitIsEvenTrue();
        lastDigitIsEvenFalse();
        lastDigitIsOddTrue();
        lastDigitIsOddFalse();
    }

    public static void lastDigitIsEvenTrue() {
        assertEquals("TaskCh04N033Test.lastDigitIsEvenTrue", true, lastDigitIsEven(2));
    }

    public static void lastDigitIsEvenFalse() {
        assertEquals("TaskCh04N033Test.lastDigitIsEvenFalse", false, lastDigitIsEven(1));
    }

    public static void lastDigitIsOddTrue() {
        assertEquals("TaskCh04N033Test.lastDigitIsOddTrue", true, lastDigitIsOdd(1));
    }

    public static void lastDigitIsOddFalse() {
        assertEquals("TaskCh04N033Test.lastDigitIsOddFalse", false, lastDigitIsOdd(2));
    }
}
