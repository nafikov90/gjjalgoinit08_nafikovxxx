package com.getjavajob.training.algo.init.nafikovi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database database = new Database();
        database.initDatabase();
        System.out.println("Enter the name for search");
        database.printResult(database.findEmployeesByName(scanner.nextLine()));
        System.out.println("Enter the work experience for search");
        database.printResult(database.findEmployeesByExperience(scanner.nextInt()));
    }

    public static class Employee {
        private String name;
        private String surname;
        private String patronymic;
        private String adress;
        private String dateOfEmployment;

        public Employee(String name, String surname, String adress, String dateOfEmployment) {
            this.name = name;
            this.surname = surname;
            this.adress = adress;
            this.dateOfEmployment = dateOfEmployment;
        }

        public Employee(String name, String surname, String patronymic, String adress, String dateOfEmployment) {
            this(name, surname, adress, dateOfEmployment);
            this.patronymic = patronymic;
        }

        public String getName() {
            return name;
        }

        public String getSurname() {
            return surname;
        }

        public String getPatronymic() {
            if (patronymic == null) {
                return "";
            } else {
                return patronymic;
            }
        }

        public String getAdress() {
            return adress;
        }

        public String getDateOfEmployment() {
            return dateOfEmployment;
        }

        public int getRecordOfWork() {
            int monthOfEmployment = Integer.parseInt(dateOfEmployment.substring(0, 2));
            int yearOfEmployment = Integer.parseInt(dateOfEmployment.substring(3));
            Calendar cal = Calendar.getInstance();
            int month = cal.get(Calendar.MONTH) + 1;
            int year = cal.get(Calendar.YEAR);
            return month >= monthOfEmployment ? year - yearOfEmployment : year - yearOfEmployment - 1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            Employee employee = (Employee) o;

            if (name != null ? !name.equals(employee.name) : employee.name != null) {
                return false;
            }
            if (surname != null ? !surname.equals(employee.surname) : employee.surname != null) {
                return false;
            }
            if (adress != null ? !adress.equals(employee.adress) : employee.adress != null) {
                return false;
            }
            return dateOfEmployment != null ? dateOfEmployment.equals(employee.dateOfEmployment) : employee.dateOfEmployment == null;

        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (surname != null ? surname.hashCode() : 0);
            result = 31 * result + (adress != null ? adress.hashCode() : 0);
            result = 31 * result + (dateOfEmployment != null ? dateOfEmployment.hashCode() : 0);
            return result;
        }
    }

    public static class Database {
        private ArrayList<Employee> employees = new ArrayList<>();

        public void initDatabase() {
            Employee petrIvanov = new Employee("Petr", "Ivanov", "Moscow, Tverskaya 12", "11.2008");
            employees.add(petrIvanov);
            Employee igorIvanov = new Employee("Igor", "Ivanov", "Ufa,  Lenina, 33", "10.2013");
            employees.add(igorIvanov);
            Employee annaPetrova = new Employee("Anna", "Petrova", "Igorevna", "Ufa,  Gagarina 31", "05.2011");
            employees.add(annaPetrova);
            Employee olgaTikhonova = new Employee("Olga", "Tikhonova", "Ivanovna", "Tula,  Pirogova 12", "05.2012");
            employees.add(olgaTikhonova);
            Employee olegViktorov = new Employee("Oleg", "Viktorov", "Kursk,  Kurskaya, 15", "02.2007");
            employees.add(olegViktorov);
            Employee ignatKulkov = new Employee("Ignat", "Kulkov", "Viktorovich", "Bryansk,  Tikhomirova, 5", "12.2012");
            employees.add(ignatKulkov);
            Employee ruslanFadeev = new Employee("Ruslan", "Fadeev", "Omsk,  Gagarina, 30", "08.2014");
            employees.add(ruslanFadeev);
            Employee alinaKupcova = new Employee("Alina", "Kupcova", "Igorevna", "Omsk,  Lenina, 48", "01.2001");
            employees.add(alinaKupcova);
            Employee ivanPrudnikov = new Employee("Ivan", "Prudnikov", "Tomsk,  Lenina, 49", "09.2009");
            employees.add(ivanPrudnikov);
            Employee georgyLenin = new Employee("Georgy", "Lenin", "Ufa,  Lenina, 51", "07.2007");
            employees.add(georgyLenin);
            Employee denisLomov = new Employee("Denis", "Lomov", "Petrovich", "Kiev,  Marksa, 10", "10.2010");
            employees.add(denisLomov);
            Employee johnNewman = new Employee("John", "Newman", "London,  Lenina, 15", "01.2015");
            employees.add(johnNewman);
            Employee alexJames = new Employee("Alex", "James", "Samara,  Dekabristov, 1", "06.2011");
            employees.add(alexJames);
            Employee zaharPakhomov = new Employee("Zahar", "Pakhomov", "Danilovich", "Tver,  Tverskaya, 55", "04.2010");
            employees.add(zaharPakhomov);
            Employee sonyaNochnaya = new Employee("Sonya", "Nochnaya", "Igorevna", "Moscow,  Tverskaya, 17", "12.2012");
            employees.add(sonyaNochnaya);
            Employee jackJackson = new Employee("Jack", "Jackson", "Moscow,  Lublinskaya, 153", "04.2007");
            employees.add(jackJackson);
            Employee billMurrey = new Employee("Bill", "Murrey", "Los Angeles,  Lenina, 15", "01.1998");
            employees.add(billMurrey);
            Employee samirNasri = new Employee("Samir", "Nasri", "Valencia,  Lenina, 50", "08.2009");
            employees.add(samirNasri);
            Employee michaelPhelps = new Employee("Michael", "Phelps", "New-York, Lenina 15", "05.2014");
            employees.add(michaelPhelps);
            Employee wayneRooney = new Employee("Wayne", "Rooney", "Manchester, Lenina, 44", "05.2003");
            employees.add(wayneRooney);
        }

        public ArrayList<Employee> getEmployees() {
            return employees;
        }

        public void setEmployees(ArrayList<Employee> employees) {
            this.employees = employees;
        }

        public ArrayList<Employee> findEmployeesByName(String searchQuery) {
            ArrayList<Employee> list = new ArrayList<>();
            for (int i = 0; i < employees.size(); i++) {
                if (employees.get(i).getName().toLowerCase().contains(searchQuery.toLowerCase()) ||
                        employees.get(i).getSurname().toLowerCase().contains(searchQuery.toLowerCase()) ||
                        employees.get(i).getPatronymic().toLowerCase().contains(searchQuery.toLowerCase())) {
                    list.add(employees.get(i));
                }
            }
            return list;
        }

        public ArrayList<Employee> findEmployeesByExperience(int a) {
            ArrayList<Employee> list = new ArrayList<>();
            for (int i = 0; i < employees.size(); i++) {
                if (employees.get(i).getRecordOfWork() >= a) {
                    list.add(employees.get(i));
                }
            }
            return list;
        }

        public void printResult(ArrayList<Employee> list) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println("Name: " + list.get(i).getName() + " " + list.get(i).getSurname() + " " +
                        list.get(i).getPatronymic());
                System.out.println("Adress: " + list.get(i).getAdress());
                System.out.println("Date of employment: " + list.get(i).getDateOfEmployment());
                System.out.println();
            }
        }
    }
}

