package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N115.animalAndColorIs;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        yearTest1();
        yearTest2();
    }

    public static void yearTest1() {
        assertEquals("TaskCh04N115Test.yearTest1", true, animalAndColorIs(2016).equals("Monkey, Red"));
    }

    public static void yearTest2() {
        assertEquals("TaskCh04N115Test.yearTest1", true, animalAndColorIs(1940).equals("Dragon, White"));
    }
}
