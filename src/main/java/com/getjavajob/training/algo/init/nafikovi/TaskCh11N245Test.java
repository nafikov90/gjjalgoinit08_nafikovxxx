package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh11N245.replaceElements1;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testReplace1();
        testReplace2();
        testReplace3();
    }

    public static void testReplace1() {
        int[] testArrayBefore = {1, -1};
        int[] testArrayAfter = {-1, 1};
        assertEquals("TaskCh11N245Test.testReplace1", true,
                Arrays.equals(testArrayAfter, replaceElements1(testArrayBefore)));
    }

    public static void testReplace2() {
        int[] testArrayBefore = {1, -1, 1};
        int[] testArrayAfter = {-1, 1, 1};
        assertEquals("TaskCh11N245Test.testReplace2", true, Arrays.equals(testArrayAfter, replaceElements1(testArrayBefore)));
    }

    public static void testReplace3() {
        int[] testArrayBefore = {1, -1, 3, -4, 5};
        int[] testArrayAfter = {-1, -4, 5, 3, 1};
        assertEquals("TaskCh11N245Test.testReplace3", true, Arrays.equals(testArrayAfter, replaceElements1(testArrayBefore)));
    }
}
