package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N185.isBracesTrue;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N185.isExcessBraces;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        testIsBracesTrue1();
        testIsBracesTrue2();
        testExcessBraces3();
        testExcessBraces4();
    }

    public static void testIsBracesTrue1() {
        assertEquals("TaskCh09N185Test.testIsBracesTrue1", true, isBracesTrue("()(())()"));
    }

    public static void testIsBracesTrue2() {
        assertEquals("TaskCh09N185Test.testIsBracesTrue2", false, isBracesTrue("((()"));
    }

    public static void testExcessBraces3() {
        assertEquals("TaskCh09N185Test.testExcessBraces3", true,
                isExcessBraces("(()))").equals("Excess closing brace 4"));
    }

    public static void testExcessBraces4() {
        assertEquals("TaskCh09N185Test.testExcessBraces4", true,
                isExcessBraces("((()").equals("Excess open brace 2"));
    }
}
