package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number k (1 <= k <=365)");
        int k = scanner.nextInt();
        String result = isWeekend(k);
        System.out.println(k + " day of the year is " + result);
    }

    public static String isWeekend(int k) {
        return (k % 7 == 0) || (k % 7 == 6) ? "Weekend" : "Workday";
    }
}
