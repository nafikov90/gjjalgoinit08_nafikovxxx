package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number n (100 <= n <= 999)");
        int number = scanner.nextInt();
        int result = findX(number);
        System.out.println(result);
    }

    public static int findX(int number) {
        return number / 100 * 100 + number % 10 * 10 + (number / 10) % 10;
    }
}
