package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh11N158.deleteIdenticalElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testDeleteElements1();
        testDeleteElements2();
        testDeleteElements3();
    }

    public static void testDeleteElements1() {
        int[] testArrayBefore = {1, 2, 1, 3, 2};
        int[] testArrayAfter = {1, 2, 3, 0, 0};
        assertEquals("TaskCh11N158Test.testDeleteElements1", true,
                Arrays.equals(testArrayAfter, deleteIdenticalElement(testArrayBefore)));
    }

    public static void testDeleteElements2() {
        int[] testArrayBefore = {1, 1};
        int[] testArrayAfter = {1, 0};
        assertEquals("TaskCh11N158Test.testDeleteElements2", true,
                Arrays.equals(testArrayAfter, deleteIdenticalElement(testArrayBefore)));
    }

    public static void testDeleteElements3() {
        int[] testArrayBefore = {1};
        int[] testArrayAfter = {1};
        assertEquals("TaskCh11N158Test.testDeleteElements3", true,
                Arrays.equals(testArrayAfter, deleteIdenticalElement(testArrayBefore)));
    }


}
