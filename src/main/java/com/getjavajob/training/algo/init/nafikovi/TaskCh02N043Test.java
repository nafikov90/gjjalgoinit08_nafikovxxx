package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh02N043.isDivisor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivisorTrue();
        testDivisorFalse();
    }

    public static void testDivisorTrue() {
        assertEquals("TaskCh02N043Test.testDivisorTrue", 1, isDivisor(2, 4));
    }

    public static void testDivisorFalse() {
        assertEquals("TaskCh02N043Test.testDivisorFalse", 3, isDivisor(2, 5));
    }
}
