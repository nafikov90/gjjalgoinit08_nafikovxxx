package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N106.whatIsSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        seasonTest1();
        seasonTest2();
        seasonTest3();
        seasonTest4();
        seasonTest5();
        seasonTest6();
        seasonTest7();
        seasonTest8();
        seasonTest9();
        seasonTest10();
        seasonTest11();
        seasonTest12();
        seasonTest13();
    }

    public static void seasonTest1() {
        assertEquals("TaskCh04N106Test.seasonTest1", true, whatIsSeason(1).equals("Winter"));
    }

    public static void seasonTest2() {
        assertEquals("TaskCh04N106Test.seasonTest2", true, whatIsSeason(2).equals("Winter"));
    }

    public static void seasonTest3() {
        assertEquals("TaskCh04N106Test.seasonTest3", true, whatIsSeason(3).equals("Spring"));
    }

    public static void seasonTest4() {
        assertEquals("TaskCh04N106Test.seasonTest4", true, whatIsSeason(4).equals("Spring"));
    }

    public static void seasonTest5() {
        assertEquals("TaskCh04N106Test.seasonTest5", true, whatIsSeason(5).equals("Spring"));
    }

    public static void seasonTest6() {
        assertEquals("TaskCh04N106Test.seasonTest6", true, whatIsSeason(6).equals("Summer"));
    }

    public static void seasonTest7() {
        assertEquals("TaskCh04N106Test.seasonTest7", true, whatIsSeason(7).equals("Summer"));
    }

    public static void seasonTest8() {
        assertEquals("TaskCh04N106Test.seasonTest8", true, whatIsSeason(8).equals("Summer"));
    }

    public static void seasonTest9() {
        assertEquals("TaskCh04N106Test.seasonTest9", true, whatIsSeason(9).equals("Autumn"));
    }

    public static void seasonTest10() {
        assertEquals("TaskCh04N106Test.seasonTest10", true, whatIsSeason(10).equals("Autumn"));
    }

    public static void seasonTest11() {
        assertEquals("TaskCh04N106Test.seasonTest11", true, whatIsSeason(11).equals("Autumn"));
    }

    public static void seasonTest12() {
        assertEquals("TaskCh04N106Test.seasonTest12", true, whatIsSeason(12).equals("Winter"));
    }

    public static void seasonTest13() {
        assertEquals("TaskCh04N106Test.seasonTest13", true, whatIsSeason(13).equals("Input Error"));
    }
}
