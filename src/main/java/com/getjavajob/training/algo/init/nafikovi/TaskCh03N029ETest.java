package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029E.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029ETest {
    public static void main(String[] args) {
        testIsTrue1();
        testIsTrue2();
        testIsTrue3();
        testIsTrue4();
        testIsTrue5();
        testIsTrue6();
        testIsTrue7();
        testIsTrue8();
    }

    public static void testIsTrue1() {
        assertEquals("TaskCh03N029FTest.testIsTrue1", false, isTrue(1, 2, 3));
    }

    public static void testIsTrue2() {
        assertEquals("TaskCh03N029ETest.testIsTrue2", true, isTrue(1, 2, 5));
    }

    public static void testIsTrue3() {
        assertEquals("TaskCh03N029ETest.testIsTrue3", true, isTrue(0, 10, 8));
    }

    public static void testIsTrue4() {
        assertEquals("TaskCh03N029ETest.testIsTrue4", false, isTrue(51, 55, 15));
    }

    public static void testIsTrue5() {
        assertEquals("TaskCh03N029ETest.testIsTrue5", true, isTrue(10, 1, 2));
    }

    public static void testIsTrue6() {
        assertEquals("TaskCh03N029ETest.testIsTrue6", false, isTrue(10, 1, 15));
    }

    public static void testIsTrue7() {
        assertEquals("TaskCh03N029ETest.testIsTrue7", false, isTrue(10, 20, 2));
    }

    public static void testIsTrue8() {
        assertEquals("TaskCh03N029ETest.testIsTrue8", false, isTrue(10, 50, 100));
    }
}
