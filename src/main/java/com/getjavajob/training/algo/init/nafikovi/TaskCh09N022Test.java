package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N022.halfOfWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        testHalfWord();
    }

    public static void testHalfWord() {
        assertEquals("TaskCh09N022Test.testHalfWord", true, halfOfWord("test").equals("te"));
    }
}
