package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh05N064 {
    private static final int COUNT_OF_DISTRICTS = 12;
    @SuppressWarnings("CanBeFinal")
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] population = populationOf12Districts();
        int[] square = squareOf12Districts();
        double result = averageDensity(population, square);
        System.out.println(result);
    }

    public static int[] populationOf12Districts() {
        System.out.println("enter the population (in thousands) of 12 districts");
        int[] population = new int[COUNT_OF_DISTRICTS];
        for (int i = 0; i < population.length; i++) {
            population[i] = scanner.nextInt();
        }
        return population;
    }

    public static int[] squareOf12Districts() {
        System.out.println("enter the square (in km2) of 12 districts");
        int[] square = new int[COUNT_OF_DISTRICTS];
        for (int i = 0; i < square.length; i++) {
            square[i] = scanner.nextInt();
        }
        return square;
    }

    public static double averageDensity(int[] population, int[] square) {
        int sumPopulation = 0;
        int sumSquare = 0;
        for (int i = 0; i < population.length; i++) {
            sumPopulation += population[i];
            sumSquare += square[i];
        }
        return sumPopulation / sumSquare;
    }
}
