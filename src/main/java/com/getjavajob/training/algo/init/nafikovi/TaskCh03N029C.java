package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh03N029C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int x = scanner.nextInt();
        System.out.println("Enter the number");
        int y = scanner.nextInt();
        boolean result = isTrue(x, y);
        System.out.println(result);
    }

    public static boolean isTrue(int x, int y) {
        return x == 0 || y == 0;
    }
}
