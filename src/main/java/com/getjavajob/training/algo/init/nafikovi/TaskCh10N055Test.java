package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N055.method1;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N055.resetValueOfString;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        test16x();
        test8x();
        test4x();
        test2x();
    }

    public static void test16x() {
        assertEquals("TaskCh10N055Test.test16x", 64, method1(16, 100));
    }

    public static void test8x() {
        resetValueOfString();
        assertEquals("TaskCh10N055Test.test8x", 101, method1(8, 65));
    }

    public static void test4x() {
        resetValueOfString();
        assertEquals("TaskCh10N055Test.test16x", 121, method1(4, 25));
    }

    public static void test2x() {
        resetValueOfString();
        assertEquals("TaskCh10N055Test.test16x", 1000, method1(2, 8));
    }
}
