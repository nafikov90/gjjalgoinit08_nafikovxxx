package com.getjavajob.training.algo.init.nafikovi;

import java.util.Arrays;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh06N008.calculate;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testArrays();
    }

    public static void testArrays() {
        int[] testArray = {1, 4, 9};
        assertEquals("TaskCh06N008Test.testArrays", true, Arrays.equals(testArray, calculate(10)));
    }
}
