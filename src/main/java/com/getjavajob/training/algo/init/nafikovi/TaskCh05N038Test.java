package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh05N038.distanceFromHome;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh05N038.totalDistance;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        testDistanceFromHome();
        testTotalDistance();
    }

    public static void testDistanceFromHome() {
        assertEquals("TaskCh05N038Test.testDistanceFromHome", true, distanceFromHome().equals("0,688172"));
    }

    public static void testTotalDistance() {
        assertEquals("TaskCh05N038Test.testTotalDistance", true, totalDistance().equals("5,187378"));
    }

}
