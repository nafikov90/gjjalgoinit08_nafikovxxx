package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the natural number a");
        int number = scanner.nextInt();
        int result = findDigitRoot(number);
        System.out.println(result);
    }

    public static int findDigitRoot(int number) {
        if (number < 10) {
            return number;
        }
        return findDigitRoot(calcSumOfDigit(number));
    }


    public static int calcSumOfDigit(int number) {
        if (number / 10 == 0) {
            return number % 10;
        }
        return number % 10 + calcSumOfDigit(number / 10);
    }
}
