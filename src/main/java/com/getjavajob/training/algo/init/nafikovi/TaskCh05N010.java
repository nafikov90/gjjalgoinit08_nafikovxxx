package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the USD/RUB exchange rate");
        double exchangeRate = scanner.nextDouble();
        double[] result = calculateUsdToRub(exchangeRate);
        for (double d : result) {
            System.out.println(d);
        }
    }

    public static double[] calculateUsdToRub(double exchangeRate) {
        double[] usdToRub = new double[20];
        for (int i = 0; i < 20; i++) {
            usdToRub[i] = (i + 1) * exchangeRate;
        }
        return usdToRub;
    }
}
