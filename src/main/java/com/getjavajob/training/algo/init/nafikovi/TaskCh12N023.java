package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the odd number");
        int n = scanner.nextInt();
        int[][] resultA = fillTheArrayA(n);
        printArray(resultA);
        int[][] resultB = fillTheArrayB(n);
        printArray(resultB);
        int[][] resultC = fillTheArrayC(n);
        printArray(resultC);
    }


    public static int[][] fillTheArrayA(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (j == i || j + i == arr.length - 1) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayB(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (j == i || j + i == arr.length - 1 || i == n / 2 || j == n / 2) {
                    arr[i][j] = 1;
                }
            }
        }
        return arr;
    }

    public static int[][] fillTheArrayC(int n) {
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i <= n / 2) {
                    if (j >= i && j <= arr.length - 1 - i) {
                        arr[i][j] = 1;
                    }
                } else {
                    if (j <= i && j >= arr.length - 1 - i) {
                        arr[i][j] = 1;
                    }
                }
            }
        }
        return arr;
    }

    public static void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
