package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N056.isPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsPrime1();
        testIsPrime2();
        testIsPrime3();
        testIsPrime4();
    }

    public static void testIsPrime1() {
        assertEquals("TaskCh10N056Test.testIsPrime1", true, isPrime(1, 0));
    }

    public static void testIsPrime2() {
        assertEquals("TaskCh10N056Test.testIsPrime2", true, isPrime(3, 2));
    }

    public static void testIsPrime3() {
        assertEquals("TaskCh10N056Test.testIsPrime3", false, isPrime(4, 3));
    }

    public static void testIsPrime4() {
        assertEquals("TaskCh10N056Test.testIsPrime4", true, isPrime(11, 10));
    }
}
