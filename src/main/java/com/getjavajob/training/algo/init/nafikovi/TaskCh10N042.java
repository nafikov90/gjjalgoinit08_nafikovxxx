package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number a");
        int a = scanner.nextInt();
        System.out.println("Enter the power n");
        int n = scanner.nextInt();
        int result = raiseToPower(a, n);
        System.out.println(result);
    }

    public static int raiseToPower(int a, int n) {
        int result;
        if (n == 0) {
            return 1;
        }
        result = a * raiseToPower(a, n - 1);
        return result;
    }
}
