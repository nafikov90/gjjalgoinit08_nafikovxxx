package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N048 {
    static final int ZERO_INDEX = 0;
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the amount of elements in the array");
        int arrayLength = scanner.nextInt();
        int maxEl = maxElemOfArray(createArray(arrayLength), ZERO_INDEX);
        System.out.println("Maximum element of the array: " + maxEl);
    }

    public static int maxElemOfArray(int[] array, int index) {
        if (index == array.length - 1) {
            return array[index];
        }
        int max = maxElemOfArray(array, index + 1);
        return array[index] < max ? max : array[index];
    }

    public static int[] createArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Enter " + i + " element of the array");
            array[i] = scanner.nextInt();
        }
        return array;
    }
}
