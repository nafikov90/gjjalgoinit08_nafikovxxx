package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N048.ZERO_INDEX;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N048.maxElemOfArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testMaxElem1();
        testMaxElem2();
        testMaxElem3();
        testMaxElem4();
    }

    public static void testMaxElem1() {
        int[] array = {0, 0, 0, 1};
        assertEquals("TaskCh10N048Test.testMaxElem1", 1, maxElemOfArray(array, ZERO_INDEX));
    }

    public static void testMaxElem2() {
        int[] array = {1, 0, 0, 0};
        assertEquals("TaskCh10N048Test.testMaxElem2", 1, maxElemOfArray(array, ZERO_INDEX));
    }

    public static void testMaxElem3() {
        int[] array = {1, 0, 0, 1};
        assertEquals("TaskCh10N048Test.testMaxElem3", 1, maxElemOfArray(array, ZERO_INDEX));
    }

    public static void testMaxElem4() {
        int[] array = {1, 10, 2, 7, 5};
        assertEquals("TaskCh10N048Test.testMaxElem4", 10, maxElemOfArray(array, ZERO_INDEX));
    }
}
