package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N043.calcCountOfDigit;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N043.calcSumOfDigit;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testSum1();
        testSum2();
        testCount3();
        testCount4();
    }

    public static void testSum1() {
        assertEquals("TaskCh10N043Test.testSum1", 5, calcSumOfDigit(5));
    }

    public static void testSum2() {
        assertEquals("TaskCh10N043Test.testSum2", 6, calcSumOfDigit(123));
    }

    public static void testCount3() {
        assertEquals("TaskCh10N043Test.testCount3", 1, calcCountOfDigit(5));
    }

    public static void testCount4() {
        assertEquals("TaskCh10N043Test.testCount4", 3, calcCountOfDigit(123));
    }
}
