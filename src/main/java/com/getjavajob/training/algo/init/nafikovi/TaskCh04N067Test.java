package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N067.isWeekend;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        weekendTest1();
        weekendTest2();
    }

    public static void weekendTest1() {
        assertEquals("TaskCh04N067Test.weekendTest1", true, isWeekend(5).equals("Workday"));
    }

    public static void weekendTest2() {
        assertEquals("TaskCh04N067Test.weekendTest2", true, isWeekend(7).equals("Weekend"));
    }
}
