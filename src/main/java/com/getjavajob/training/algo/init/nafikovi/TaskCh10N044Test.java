package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N044.findDigitRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        testDigitRoot1();
        testDigitRoot2();
    }

    public static void testDigitRoot1() {
        assertEquals("TaskCh10N044Test.testDigitRoot1", 1, findDigitRoot(1));
    }

    public static void testDigitRoot2() {
        assertEquals("TaskCh10N044Test.testDigitRoot2", 9, findDigitRoot(9999));
    }
}
